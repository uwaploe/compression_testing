
import pytest
import numpy as np

from compression_testing import testdata

def test_load_testdata():

    td = testdata.TestData()

    sets = td.sets()
    assert len(sets) > 0


def test_testdata_draw():

    td = testdata.TestData()

    sets = td.sets()

    set = np.random.choice(sets)

    s = td.get(set)

    to_draw =  np.random.randint(0, len(s["data"]) )
    draw = td.draw(set, to_draw )

    assert len(draw) == to_draw
    for d in draw:
        assert len(d) == s["length"]

    draw = td.draw(set)

    assert len(draw) == len(s["data"])
    for d in draw:
        assert len(d) == s["length"]
