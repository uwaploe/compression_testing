import setuptools


setuptools.setup(
    name="compression_testing",
    version="0.0.1",
    author="Aaron Marburg",
    author_email="amarburg@uw.edu",
    description="A small example package",
    long_description="A longer description",
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
