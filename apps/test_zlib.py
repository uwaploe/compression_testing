#!/usr/bin/env python3

from compression_testing import compressor_test

import zlib
from functools import partial
import json


def compress_zlib( level, inbin ):
    return zlib.compress(inbin, level)

if __name__ == '__main__':

    ct = compressor_test.CompressorTest()

    results = []

    for level in range(9):
        name = "zlib_level_" + str(level)
        result = ct.test_sets(partial(compress_zlib, level))

        results.append( {'compressor': 'zlib',
                        'compressor_level': level,
                        'name': name,
                        'results': result } )

    with open("test_zlib.json", 'w') as f:
        json.dump( results, f, indent=2 )
