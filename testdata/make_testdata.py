#!/usr/bin/env python

import numpy as np
import json
from functools import partial


def make_random_integer(length,dtype):
    return np.random.randint(np.iinfo(dtype).min, np.iinfo(dtype).max, length, dtype)

def make_random_float32(length):
    float_min = -512
    float_range = 1024
    return np.random.sample(length) * float_range + float_min

def make_entry( count, dtype, f ):
    data = [f().tolist() for i in np.arange(count)]
    data0 = data[0]

    return { 'dtype': str(dtype),
             'length': len(data[0]),
             'data': data }



if __name__ == '__main__':

    ## Fixed random seed for debugging
    np.random.seed(0)

    output_file = "test_data.json"

    count = 10
    lengths = [10, 50, 100]

    test_data = {}

    int_dtypes = [np.uint8, np.int16, np.int32]
    int_dtypes = [np.dtype(dt) for dt in int_dtypes]

    for length in lengths:
        for dtype in int_dtypes:
            name = "random_" + str(dtype) + "_" + str(length)
            test_data[name] = make_entry(count, dtype, partial(make_random_integer, length, dtype) )


        name = "random_float32_" + str(length)
        test_data[name] = make_entry(count, np.dtype(np.float32), partial(make_random_float32, length) )

    with open( output_file, 'w' ) as f:
        json.dump( test_data, f, indent=2 )


    # # Dict to accumulate test data.  Serialized to file at end of main()
    # test_data = {}
    #
    # ## Detector sampling freq and length of buffer
    # data_hz=10
    # buffer_len_secs=6
    #
    #
    #
    # buffer_len = data_hz*buffer_len_secs
    #
    # # ***** Generate the all-zeroes data set
    # test_data['zeros'] = mat_to_json( np.zeros( (buffer_len), dtype=np.int8 ) )
    #
    #
    # # ***** Generate a random data set
    # test_data['random'] = mat_to_json( np.random.randint(0,2, size=(buffer_len), dtype=np.int8 ) )
    #
    # # ***** Generate other data sets here...
    #
    #
    # ### Write test_data to file
    # with open( "pl_example_test_data.json", 'w' ) as f:
    #   json.dump( test_data, f, indent=2 )
