
from . import testdata
import numpy as np

class CompressorTest:

    def __init__( self, testdata = testdata.TestData() ):
        self.testdata = testdata

    def test_sets( self, func, sets=None, count=None ):

        if sets is None:
            sets = self.testdata.sets()

        return [self.test_set( func, set ) for set in sets]

    def test_set( self, func, set, count=None ):

        if set not in self.testdata.sets():
            return {}

        input = self.testdata.draw(set,count)

        results = []

        for i in input:
            inbin = np.asarray(i,dtype=self.testdata.dtype(set)).tobytes()
            inbinlen = len(inbin)

            outbin = func(inbin)
            results.append( {'in_length': inbinlen,
                             'out_length': len(outbin)} )

        out = { 'test_set': set,
                'results': results }

        return out
