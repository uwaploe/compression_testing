

import json
from pathlib import Path
import numpy as np


class TestData:

    def __init__( self, filename=Path(__file__).parent.parent.joinpath('testdata/test_data.json')):

        with open(filename) as f:
            self.td = json.load( f )

    def sets( self ):
        return list(self.td.keys())

    # def lengths( self, dtype ):
    #     if dtype not in self.dtypes():
    #         return None
    #
    #     return [ int(a["length"]) for a in self.td[dtype] ]

    def get( self, key ):
        return self.td[key]

    def dtype(self, key):
        return self.get(key)['dtype']

    def draw( self, key, count=None ):
        s = self.get(key)

        if count is None:
            return s["data"]

        idxs = np.random.randint(0,len(s["data"]),(count) )
        return [s["data"][i] for i in idxs]
